package patchara.databaseproject;

import patchara.databaseproject.model.User;
import patchara.databaseproject.service.UserService;

public class TestUserService {

    public static void main(String[] args) {
        UserService userService = new UserService();
        User user = userService.login("patchara", "qwer");
        if (user != null) {
            System.out.println("Welcome user: " + user.getName());
        }else{
            System.out.println("Error");
        }
    }
}
