
package patchara.databaseproject;

import patchara.databaseproject.dao.UserDao;
import patchara.databaseproject.helper.DatabaseHelper;
import patchara.databaseproject.model.User;

public class TestUserDao {
    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        
        for(User u:userDao.getAll()){
            System.out.println(u);
        }
        System.out.println();
//        User user1 = userDao.get(3);
//        System.out.println(user1+"\n");
        
//        User user = new User("Suphasara","qqq",2,"F");
//        User inserted = userDao.save(user);
//        System.out.println(inserted+"\n");
//        
//        user1.setGender("F");
//        userDao.update(user1);
//        User updateUser = userDao.get(user1.getId());
//        System.out.println(updateUser+"\n");
//        
//        userDao.delete(user1);
//        for(User u:userDao.getAll()){
//            System.out.println(u);
//        }
        System.out.println();
        for(User u:userDao.getAll("user_name LIKE 'p%' ", " user_id ASC ")){
            System.out.println(u);
        }
        DatabaseHelper.close();
        
    }
}
