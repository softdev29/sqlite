BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "product" (
	"product_id"	INTEGER,
	"product_name"	TEXT(50) NOT NULL UNIQUE,
	"product_price"	DOUBLE NOT NULL,
	"product_size"	TEXT(5) DEFAULT 'SML',
	"product_sweet_level"	INTEGER DEFAULT 0123,
	"product_type"	TEXT(5) DEFAULT 'HCF',
	"category_id"	INTEGER(5) NOT NULL,
	PRIMARY KEY("product_id"),
	FOREIGN KEY("category_id") REFERENCES "category"("category_id")
);
CREATE TABLE IF NOT EXISTS "category" (
	"category_id"	INTEGER,
	"category_name"	TEXT(50) NOT NULL,
	PRIMARY KEY("category_id")
);
INSERT INTO "product" ("product_id","product_name","product_price","product_size","product_sweet_level","product_type","category_id") VALUES (1,'Americano',40.0,'SML',123,'HC',1),
 (2,'Cappucino',40.0,'SML',123,'HCF',1),
 (3,'Latte',40.0,'SML',123,'HCF',1),
 (4,'Mocha',40.0,'SML',123,'HCF',1),
 (5,'Espresso',40.0,'SML',123,'HCF',1),
 (6,'Green tea',40.0,'SML',123,'HCF',1),
 (7,'Chocolate',40.0,'SML',123,'HCF',1),
 (8,'Cheese cake',55.0,'-','-','-',2),
 (9,'Chocolate Cake',60.0,'-','-','-',2),
 (10,'Strawberry shortcake',55.0,'-','-','-',2);
INSERT INTO "category" ("category_id","category_name") VALUES (1,'Drink'),
 (2,'Dessert');
COMMIT;
